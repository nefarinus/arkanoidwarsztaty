﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public bool isBallLaunched;
    public GameObject ballPrefab;
    public Transform paddleHolder;
    public Vector3 spawnPosition;
    public Vector3 launchDirection;
    public float launchVelocity;

    public float destroyBorder;
    public LifeController lifeController;

    private Ball ball;

    private void Awake()
    {
        SpawnBall();
    }

    public void SpawnBall()
    {
        isBallLaunched = false;
        ball = Instantiate(ballPrefab, paddleHolder).GetComponent<Ball>();
        ball.transform.localPosition = spawnPosition;

    }

    public void LaunchBall()
    {
        isBallLaunched = true;
        ball.transform.parent = null;
        Vector3 newBallVelocity = new Vector3(launchDirection.x * Input.GetAxis("Horizontal"), launchDirection.y, 0).normalized * launchVelocity;
        ball.SetVelocity(newBallVelocity);
    }

    private void Update()
    {
        AttemptBallLaunch();
        CheckBallLossCondition();
    }

    private void CheckBallLossCondition()
    {
        if (ball != null && ball.transform.position.y < destroyBorder)
        {
            lifeController.LoseLife();
            Destroy(ball.gameObject);
            if (lifeController.livesRemaining >= 0)
            {
                SpawnBall();
            }
        }
    }

    private void AttemptBallLaunch()
    {
        if (!isBallLaunched && Input.GetKeyDown(KeyCode.Space))
        {
            LaunchBall();
        }
    }
}
