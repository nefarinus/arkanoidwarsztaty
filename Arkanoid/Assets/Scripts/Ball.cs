﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public Rigidbody rigidbody;

    private Vector3 currentVelocity;

    public void SetVelocity(Vector3 newVelocity)
    {
        currentVelocity = newVelocity;
        rigidbody.velocity = currentVelocity;
    }

    private void OnCollisionEnter(Collision other)
    {
        ICustomReflection customReflection = other.gameObject.GetComponent<ICustomReflection>();
        Vector3 newVelocity = Vector3.zero;

        if (customReflection != null)
        {
            newVelocity = customReflection.GetReflectedVelocity(currentVelocity, other.GetContact(0).normal);
        }
        else
        {
            newVelocity = GetDefaultBallReflection(other);
        }

        SetVelocity(newVelocity);
    }

    private Vector3 GetDefaultBallReflection(Collision other)
    {
        Vector3 normal = other.GetContact(0).normal;
        return Vector3.Reflect(currentVelocity, normal);
    }
}
