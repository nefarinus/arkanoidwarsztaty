using UnityEngine;

public interface ICustomReflection
{
    Vector3 GetReflectedVelocity(Vector3 currentVelocity, Vector3 normal);
}