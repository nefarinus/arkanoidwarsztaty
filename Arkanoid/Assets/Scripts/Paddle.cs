﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public float speed;
    public float movementAreaWidth;


    private void Update()
    {


        float positionChange = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        if (!(transform.position.x + positionChange > movementAreaWidth / 2 || transform.position.x + positionChange < -movementAreaWidth / 2))
        {
            transform.position += new Vector3(positionChange, 0, 0);
        }

    }
}
