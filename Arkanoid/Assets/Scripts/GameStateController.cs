﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateController : MonoBehaviour
{
    public LifeController lifeController;
    public BlockSetController blockSetController;
    private bool InGameState = true;
    public Text UIText;

    private void Update()
    {
        if (InGameState)
        {
            CheckWinCondition();
            CheckLoseCondition();
        }
    }

    private void CheckWinCondition()
    {
        if (blockSetController.GetBlocksRemainingCount() == 0)
        {
            InGameState = false;
            Win();
        }
    }

    private void CheckLoseCondition()
    {
        if (lifeController.livesRemaining < 0)
        {
            InGameState = false;
            Lose();
        }
    }

    private void Lose()
    {
        UIText.text = "Game Over";
        Debug.Log("Game Over");
    }

    private void Win()
    {
        UIText.text = "Game Won";
        Debug.Log("Game Won");
    }
}

