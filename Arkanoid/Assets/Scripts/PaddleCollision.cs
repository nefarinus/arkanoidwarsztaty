﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleCollision : MonoBehaviour, ICustomReflection
{
    public float movementModiffier;

    public Vector3 GetReflectedVelocity(Vector3 currentVelocity, Vector3 normal)
    {
        Vector3 newV = Vector3.Reflect(currentVelocity, normal);
        float xModiffier = Input.GetAxis("Horizontal") * movementModiffier;
        newV += new Vector3(xModiffier, 0, 0);

        return newV;
    }
}
