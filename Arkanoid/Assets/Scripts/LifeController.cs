﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeController : MonoBehaviour
{
    public int maxLives;
    public int livesRemaining;
    public Text lifeUIText;


    private void Awake()
    {
        livesRemaining = maxLives;
        UpdateUIText();
    }

    public void LoseLife()
    {
        livesRemaining--;

        UpdateUIText();
    }

    public void GainLife()
    {
        livesRemaining++;

        livesRemaining = Mathf.Clamp(livesRemaining, 0, maxLives);

        UpdateUIText();
    }

    private void UpdateUIText()
    {
        if (livesRemaining >= 0)
        {
            lifeUIText.text = livesRemaining + " Lives Remaining";
        }
        else
        {
            lifeUIText.text = "";
        }
    }
}
