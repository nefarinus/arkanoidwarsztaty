﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSetController : MonoBehaviour
{
    public int blocksInRowCount;
    public int rowsCount;
    public float verticalOffset;
    public float horizontalOffset;

    public GameObject blockPrefab;
    public Transform BlocksHolder;

    private List<GameObject> blocks = new List<GameObject>();


    private void Awake()
    {
        SpawnBlockSet();
    }

    private void RemoveDestroyedBlocks()
    {
        blocks.RemoveAll(item => item == null);
    }

    private void SpawnBlockSet()
    {
        float rowStartPoint = -((blocksInRowCount - 1) * horizontalOffset / 2);

        for (int i = 0; i < rowsCount; ++i)
        {
            for (int j = 0; j < blocksInRowCount - (i % 2); ++j)
            {
                float x = rowStartPoint + (i % 2) * (horizontalOffset / 2) + j * horizontalOffset;
                float y = i * verticalOffset;
                Vector3 blockPosition = new Vector3(x, y, 0);

                blocks.Add(Instantiate(blockPrefab, blockPosition, Quaternion.identity, BlocksHolder));
            }
        }

    }

    public int GetBlocksRemainingCount()
    {
        return blocks.Count;
    }

    private void Update()
    {
        RemoveDestroyedBlocks();
    }

}
